#include <stdio.h>
#include <string>
#include <vector>
#include <map>
#include <Rcpp.h>
#include <iostream>
#include <fstream>


using namespace Rcpp;

std::map< char, std::string >acodes;

std::string MakeRevComplement(std::string init_str);
	

class Primer {
	
	public :
		Primer() {};
		int region;
		std::string name;
		std::string forward_template;
		std::string reverse_template;
	
		std::vector< std::string > GetForward() {
			return forward;
		}
		
		std::vector< std::string > GetReverse() {
			return reverse;
		}
		
		void MakeForwardFromTemplate( int i, std::string tstr, std::string primer_string ) {
			std::string tt = acodes[primer_string[i]];
			for(int k = 0; k < tt.length(); ++k) {
					if(i < primer_string.length()-1) {
						MakeForwardFromTemplate(i+1,tstr + tt[k], primer_string);
					} else {
						forward.push_back(tstr);
					}
			}

		}
		
		void MakeReverseFromTemplate( int i, std::string tstr, std::string primer_string ) {
			std::string tt = acodes[primer_string[i]];
			for(int k = 0; k < tt.length(); ++k) {
					if(i < primer_string.length()-1) {
						MakeReverseFromTemplate(i+1,tstr + tt[k], primer_string);
					} else {
						reverse.push_back(MakeRevComplement(tstr));
					}
			}
		}
		
	private :
		std::vector< std::string > forward;
		std::vector< std::string > reverse;
		
	
	

};

std::map< int, std::vector<Primer*> > primers;

std::string MakeRevComplement(std::string init_str) {
    
    std::reverse(init_str.begin(), init_str.end());
    
    for(int i = 0; i<(int)init_str.length(); i++) {
        if(init_str[i] == 'A') {
            init_str[i] = 'T';
            continue;
        }
        if(init_str[i] == 'T') {
            init_str[i] = 'A';
            continue;
        }
        if(init_str[i] == 'G') {
            init_str[i] = 'C';
            continue;
        }
        if(init_str[i] == 'C') {
            init_str[i] = 'G';
            continue;
        }
        
    }
    
    return init_str;
}


std::map<char, std::string> Init() {
	//This is an ambiguity codes:
	std::map< char, std::string >acodes;
	acodes['A'] = "A";
	acodes['T'] = "T";
	acodes['G'] = "G";
	acodes['C'] = "C";
	acodes['K'] = "GT";
	acodes['M'] = "AC";
	acodes['R'] = "AG";
	acodes['Y'] = "CT";
	acodes['S'] = "CG";
	acodes['W'] = "AT";
	acodes['B'] = "CGT";
	acodes['V'] = "ACG";
	acodes['H'] = "ACT";
	acodes['D'] = "AGT";
	acodes['X'] = "AGCT";
	acodes['N'] = "AGCT";
	
	for(int i=1; i<=9; ++i) {
		std::vector<Primer*> p;
		primers[i] = p;
	}
	
	return acodes;
}

//Convert string to upper case
void stoupper(std::string& s)	{
        std::string::iterator i = s.begin();
        std::string::iterator end = s.end();

        while (i != end) {
                *i = std::toupper((unsigned char)*i);
                ++i;
        }
}

void AddPrimer( DataFrame df ) {

 Rcpp::CharacterVector v1f = df["V1F"]; 
 Rcpp::CharacterVector v1r = df["V1R"];  
	
 Rcpp::CharacterVector v2f = df["V2F"]; 
 Rcpp::CharacterVector v2r = df["V2R"];  
    	
 Rcpp::CharacterVector v3f = df["V3F"]; 
 Rcpp::CharacterVector v3r = df["V3R"];  
    	
 Rcpp::CharacterVector v4f = df["V4F"]; 
 Rcpp::CharacterVector v4r = df["V4R"];  
    	
 Rcpp::CharacterVector v5f = df["V5F"]; 
 Rcpp::CharacterVector v5r = df["V5R"];  
    	
 Rcpp::CharacterVector v6f = df["V6F"]; 
 Rcpp::CharacterVector v6r = df["V6R"];  
    
 Rcpp::CharacterVector v7f = df["V7F"]; 
 Rcpp::CharacterVector v7r = df["V7R"];  
    	
 Rcpp::CharacterVector v8f = df["V8F"]; 
 Rcpp::CharacterVector v8r = df["V8R"];  
    	
 Rcpp::CharacterVector v9f = df["V9F"]; 
 Rcpp::CharacterVector v9r = df["V9R"];  

 for(int i=0; i<df.nrows(); ++i) {	
  	//V1		
	Primer *p = new Primer();
	if(v1f[i] != "NA") {
		//std::cout << Rcpp::CharacterVector(cx[i]) << '\n';
		p->forward_template = v1f[i];
		p->MakeForwardFromTemplate(0,"",p->forward_template);
	} else {
		p->forward_template = v1f[i];
	}
	if(v1r[i] != "NA") {
		p->reverse_template = v1r[i];
		p->MakeReverseFromTemplate(0, "", p->reverse_template);
	} else {
		p->reverse_template = v1r[i];
	}
	primers[1].push_back( p );  
		
	//V2		
	p = new Primer();
	if(v2f[i] != "NA") {
		//std::cout << Rcpp::CharacterVector(cx[i]) << '\n';
		p->forward_template = v2f[i];
		p->MakeForwardFromTemplate(0,"",p->forward_template);
	} else {
		p->forward_template = v2f[i];
	}
	if(v2r[i] != "NA") {
		p->reverse_template = v2r[i];
		p->MakeReverseFromTemplate(0, "", p->reverse_template);
	} else {
		p->reverse_template = v2r[i];
	}
	primers[2].push_back( p );  
		
	//V3		
	p = new Primer();
	if(v3f[i] != "NA") {
		p->forward_template = v3f[i];
		p->MakeForwardFromTemplate(0,"",p->forward_template);
	} else {
		p->forward_template = v3f[i];
	}
	if(v3r[i] != "NA") {
		p->reverse_template = v3r[i];
		p->MakeReverseFromTemplate(0, "", p->reverse_template);
	} else {
		p->reverse_template = v3r[i];
	}
	primers[3].push_back( p );  
		
	//V4		
	p = new Primer();
	if(v4f[i] != "NA") {
		p->forward_template = v4f[i];
		p->MakeForwardFromTemplate(0,"",p->forward_template);
	} else {
		p->forward_template = v4f[i];
	}
	if(v4r[i] != "NA") {
		p->reverse_template = v4r[i];
		p->MakeReverseFromTemplate(0, "", p->reverse_template);
	} else {
		p->reverse_template = v4r[i];
	}
	primers[4].push_back( p );
		
	//V5		
	p = new Primer();
	if(v5f[i] != "NA") {
		p->forward_template = v5f[i];
		p->MakeForwardFromTemplate(0,"",p->forward_template);
	} else {
		p->forward_template = v5f[i];
	}
	if(v5r[i] != "NA") {
		p->reverse_template = v5r[i];
		p->MakeReverseFromTemplate(0, "", p->reverse_template);
	} else {
		p->reverse_template = v5r[i];
	}
	primers[5].push_back( p );  
		
	//V6		
	p = new Primer();
	if(v6f[i] != "NA") {
		//std::cout << Rcpp::CharacterVector(cx[i]) << '\n';
		p->forward_template = v6f[i];
		p->MakeForwardFromTemplate(0,"",p->forward_template);
	} else {
		p->forward_template = v6f[i];
	}
	if(v6r[i] != "NA") {
		p->reverse_template = v6r[i];
		p->MakeReverseFromTemplate(0, "", p->reverse_template);
	} else {
		p->reverse_template = v6r[i];
	}
	primers[6].push_back( p );  
		
	//V7		
	p = new Primer();
	if(v7f[i] != "NA") {
		//std::cout << Rcpp::CharacterVector(cx[i]) << '\n';
		p->forward_template = v7f[i];
		p->MakeForwardFromTemplate(0,"",p->forward_template);
	} else {
		p->forward_template = v6f[i];
	}
	if(v7r[i] != "NA") {
		p->reverse_template = v7r[i];
		p->MakeReverseFromTemplate(0, "", p->reverse_template);
	} else {
		p->reverse_template = v7r[i];
	}
	primers[7].push_back( p ); 
		
	//V8		
	p = new Primer();
	if(v8f[i] != "NA") {
		p->forward_template = v8f[i];
		p->MakeForwardFromTemplate(0,"",p->forward_template);
	} else {
		p->forward_template = v8f[i];
	}
	if(v8r[i] != "NA") {
		p->reverse_template = v8r[i];
		p->MakeReverseFromTemplate(0, "", p->reverse_template);
	} else {
		p->reverse_template = v8r[i];
	}
	primers[8].push_back( p );  
	
	//V9		
	p = new Primer();
	if(v9f[i] != "NA") {
		p->forward_template = v9f[i];
		p->MakeForwardFromTemplate(0,"",p->forward_template);
	} else {
		p->forward_template = v9f[i];
	}
	if(v9r[i] != "NA") {
		p->reverse_template = v9r[i];
		p->MakeReverseFromTemplate(0, "", p->reverse_template);
	} else {
		p->reverse_template = v9r[i];
	}
	primers[9].push_back( p );
 }


}


RcppExport SEXP ExtractVRegions(SEXP filename, SEXP pr) 
{
	acodes = Init();
	
	std::map< std::string, std::string > records;
	std::map< std::string, std::string >::iterator it_records;
	
	//Builds a new Vector Dictionary. Here it assumes that frequency is 1
    std::string str;
    
    std::ifstream infile;
    
    
    //Handling primers
    Rcpp::DataFrame cx(pr);
    AddPrimer(cx);
    	
    //Open given file:
    infile.open((as<std::string>(filename)).c_str(), std::ifstream::in);
    
    if (infile.is_open()) {
    	std::cout << "Parsing file " << (as<std::string>(filename)).c_str() << std::endl;
  	} else {
    	// show message:
    	std::cout << "Error opening file: " << (as<std::string>(filename)).c_str() << '\n';
    	return R_NilValue;
  	}
    
    //Counts the number of lines (reads) in parsed file:
    long line_cnt = 0;
    //Record (read) ID
    std::string rec_id;
    std::string read;
    //Loop thru all lines in input file:
    while ( getline(infile, str) ) {
        //Determining a record (read) ID:
        if( str[0]== '>' ) 
        {
            records[rec_id] = read;            
            rec_id = str.substr(1,str.length()-1);
            read.clear();
            continue;
        }
        
        //To uppercase:
        stoupper(str);
        //Getting the whole read : 
        read.append(std::string(str));
        str.clear();
    }
    
	//The last one : 
    records[rec_id] = read;
    
    infile.close();

	//Now go thru all records and extract variable regions:
	//But before let's prepare 9 dictionaries:
	std::vector< std::map< std::string, std::string > > v_regions(9);
	
    for(it_records = records.begin(); it_records != records.end(); it_records++) {
    	read = it_records->second;
    	
    	//For v* primers:
    	for(int v=1; v<10; ++v) {
    		for(int i=0; i<primers[v].size(); ++i) {
    			if(primers[v][i]->forward_template != "NA" && primers[v][i]->reverse_template != "NA") {
    				//For each forward primer:
    				std::vector<std::string> fw = primers[v][i]->GetForward();
    				for(int j=0; j<fw.size(); ++j) {
    					std::size_t found_fw = read.find(fw[j]);
    					if (found_fw != std::string::npos) {
    						//For each reverse primer:
    						std::vector<std::string> rv = primers[v][i]->GetReverse();
    						for(int k=0; k<rv.size(); ++k) {
								std::size_t found_rv = read.find(rv[k]);
								if(found_rv != std::string::npos) {
									//std::cout << "###\n" ;
    								if(found_fw < found_rv) {
    									std::string v_region = read.substr(found_fw,found_rv-found_fw);
    									v_regions[v-1][it_records->first] = v_region;
    									break;
    								}
    							}
    						}
    					}
    				}
    			} else {
    				continue;
    			}
    		}
    	}
    	
    	
    }
    
	return Rcpp::wrap( Rcpp::List::create(records, v_regions) );
}


