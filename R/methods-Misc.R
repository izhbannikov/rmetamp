#Reads data from file contained a distance matrix:
read_data <- function(filename) {
  rawdata <- read.table(filename,skip=3)
  dm <- matrix(nrow=dim(rawdata)[1],ncol=dim(rawdata)[2]-2)
  dm <- rawdata[,3:length(rawdata)]
  rownames(dm) <- rawdata$V2
  colnames(dm) <- rawdata$V2
  #This is not efficient. Use lapply instead.
  for(i in 1:length(dm)) {
    for(j in 1:length(dm)) {
      if(i == j) {
        dm[i,j] <- 0
      } else {
        dm[i,j] <- 100-dm[i,j]
      }
    }
  }
  dm
}

add_borders <- function(MDS) {
  MDS <- rbind( MDS, c(min(MDS[,1])-0.1,min(MDS[,2]))-0.1 )
  MDS <- rbind( MDS, c(max(MDS[,1])+0.1,min(MDS[,2]))-0.1 )
  MDS <- rbind( MDS, c(max(MDS[,1])+0.1,max(MDS[,2]))+0.1 )
  MDS <- rbind( MDS, c(min(MDS[,1])-0.1,max(MDS[,2]))+0.1 )
  
  MDS
}

plot_mds <- function(mds,caption,col) {
  ## note asp = 1, to ensure Euclidean distances are represented correctly
  plot(mds[, 1], mds[, 2], asp = 1, axes = T,main = caption, col=col,cex=0.5)
  text(mds[, 1], mds[, 2], rownames(mds), cex = 0.6,pos=3)
}

#This function generates a perfect test test by getting points that a close to the reference.
#Input: coordinates of reference points and number of test points. Output: a set of points
generate_test_set <- function(dat,num,limits) {
  #dat - reference points
  #num - #of empirical points arount each reference point, i.e. a 'cloud' of points
  test_set <- matrix(0, nrow=dim(dat)[1]*num, ncol=2)
  next_index <- 1
  for(i in 1:dim(dat)[1]) {
    for(j in seq(next_index,next_index+num-1)) {
      #test_set[j,1] <- runif(1,dat[i,1]-0.025,dat[i,1]+0.025)
      #test_set[j,2] <- runif(1,dat[i,2]-0.025,dat[i,2]+0.025)
      test_set[j,1] <- runif(1,dat[i,1]-limits[1],dat[i,1]+limits[1])
      test_set[j,2] <- runif(1,dat[i,2]-limits[2],dat[i,2]+limits[2])
    }
    next_index <- next_index + num 
  }
  test_set
}

#availableClipModes <- function(object){
#  if (class(object) %in% c("SffReads","SffReadsQ", "SffReadsF")){
#    avail <- c("raw")
#    if (length(object@customIR)) avail <- c(avail,"custom")
#    if (length(object@qualityIR)) avail <- c("adapter", avail)
#    if (length(object@adapterIR)) avail <- c("quality", avail)
#    if (length(object@qualityIR) & length(object@adapterIR)) avail <- c("full",avail)
#    avail
#    ## c("full","adapter","quality","raw","custom") 
#  } else stop("clip modes only make sense for object of types SffReads or SffReadsQ classes")
#}
#
#.solveIRangeSEW <- function(widths,IR){
#  if (length(IR) == 0) return(IRanges())
#  solveUserSEW(widths,start(IR),end(IR))
#}

## Determine view on the data, called anytime one views the reads or qualitys
#"solveSffSEW" <- function(object,starts=NULL,ends=NULL,widths=NULL,clipMode)
#{
#  if (!is.null(starts) || !is.null(ends) || !is.null(widths)) { 
#    clipmode <- "Specified"
#  } else if (!missing(clipMode)) {
#    if (!(clipMode %in% availableClipModes(object)))
#      txt <- sprintf(paste("wrong mode type must be one of",paste(availableClipModes(object),collapse=",")))
#    clipmode <- clipMode
#  } else clipmode <- object@clipMode
#  
#  clipFull <- function(object){
#    clipL <- pmax(start(object@qualityIR),start(object@adapterIR))
#    clipR <- pmin(end(object@qualityIR),end(object@adapterIR))
#    return(solveUserSEW(width(object@sread),clipL,clipR))
#  }
#  switch(clipmode,
#         "specified" = solveUserSEW(width(object@sread),starts,ends,widths),
#         "custom" = .solveIRangeSEW(width(object@sread),object@customIR),
#         "full"=clipFull(object),
#         "adapter"=.solveIRangeSEW(width(object@sread),object@adapterIR),
#         "quality"=.solveIRangeSEW(width(object@sread),object@qualityIR),
#         "raw"=solveUserSEW(width(object@sread)))
#}
#
##### fix id, does not exist
#setMethod(writePhredQual, "FastqQuality", function(object, filepath, mode="w", ...) {
#  if (length(filepath) != 1)
#    sprintf("UserArgumentMismatch:'%s' must be '%s'",
#            "file", "character(1)")
#  filepath <- path.expand(filepath)
#  if (file.exists(filepath) && mode != "a")
#    sprintf("UserArgumentMismatch:file '%s' exists, but mode is not 'a'",
#            filepath)
#  ## FIXME: different quality types
#  max_width <- max( unique(width(quality(object))))
#  .Call("write_phred_quality", names(object), quality(object), file, mode, max_width)
#  invisible(length(object))
#})
