#A simple function that implements the affine transformation:
affine_transform <- function(P, Q) {
  A <- Q %*% t(P) %*% solve(P %*% t(P))
  A
}

apply_affine <- function(data,ATS,tindx) {
  A <- matrix(0,nrow=3,ncol=3)
  for(i in 1:dim(data)[1]) {
    #Here I am extracting the triangles from V4 reference plane:
    point.x <- data[i,1]
    point.y <- data[i,2]
    P <- rbind(c(point.x[1]),
               c(point.y[1]),
               c(1))
    
    A <- matrix(ATS[tindx[i,1],],nrow=3,ncol=3,byrow=T)
    
    PP <- A %*% P
    
    data[i,] <- PP[1:2,1]
  }
  data
}


process_amplicon_data <- function(mds16S, mdsV, q) {
  #========Step4: Add border points:===============#
  #In this case, use convex hull:
  rnames <- rownames(mdsV)
  chull_indices <- chull(mdsV)
  ans <- apply(mds16S[chull_indices,],2,function(x) ifelse(x<0,x-1,x+1))
  rownames(ans) <- paste(rownames(ans),"1",sep='')
  mds16S <- rbind(mds16S, ans)
  
  ans<-apply(mdsV[chull_indices,],2,function(x) ifelse(x<0,x-1,x+1))
  rownames(ans) <- paste(rownames(ans),"1",sep='')
  mdsV <- rbind(mdsV, ans)
  
  #======Step 5: Make triangulation (Use reference 16S plane for this):=======#
  ans <- triangulation(mds16S=mds16S,mdsV=mdsV)
  ATS <- ans[[1]] #A matrix that contains affine matrices for each triangle
  centroids <- ans[[2]] #This is a matrix that contain centroids and vertices of triangles
  mdsV_new <- ans[[3]][[1]] #V-reference plane after transform it back to reference 16S plane
  
  #======Step 6: Determine which empirical point belongs to which triangle:======#
  ans <- nn2(centroids[,1:2],q,k=3)
  indices <- ans[1]$nn.idx
  ##More precise estimation for each empirical point:
  #for(i in 1:dim(q)[1]) {
  #  aa <- find_triangle(q4[i,] , centroids, indices[i,],3)
  #  indices[i,1] <- ifelse(aa== -1,indices[i,1],aa)
  #}
  
  #=======Step 7: Now for each empirical point we have to apply the affine transform:======#
  q_new <- apply_affine(q,ATS,indices)
  
  
  list(mdsV_new,q_new)
}