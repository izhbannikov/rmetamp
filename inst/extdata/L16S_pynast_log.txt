candidate sequence ID	candidate nucleotide count	errors	template ID	BLAST percent identity to template	candidate nucleotide count post-NAST
L.acetotolerans 16S ribosomal RNA sequence	1518		88892	91.30	1508
L.acidophilus-johnsonii 16S ribosomal RNA (16S rRNA)	1568		164187	95.20	1408
L.amylovorus 16S ribosomal RNA	1512		88892	94.60	1502
L.bifermentans 16S ribosomal RNA	1574		15156	89.40	1531
L.curvatus 16S rRNA gene for 16S ribosomal RNA	1252		108027	92.30	1252
L.delbrueckii 16S ribosomal RNA	1512		88892	90.90	1503
L.fermentum 16S ribosomal RNA	1580		107485	93.40	1537
L.fructivorans 16S ribosomal RNA	1520		111332	91.20	1507
L.gasseri 16S ribosomal RNA	1521		164187	94.50	1407
L.lactis 16S ribosomal RNA	1569		21096	91.30	1515
L.mali KCTC 3596 = DSM 20444 16S ribosomal RNA, complete sequence	1502		111332	93.20	1491
L.oris 16S ribosomal RNA	1512		21772	96.10	1512
L.plantarum 16S ribosomal RNA	1571		15164	92.40	1553
L.ruminus 16S ribosomal RNA	1567		111332	93.10	1507
L.salivarius salicinius 16S ribosomal RNA	1512		97946	98.50	1504
L.sanfrancisco 16S ribosomal RNA	1528		26176	91.70	1516
