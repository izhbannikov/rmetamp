\examples{
#Script for meta-amplicon data analysis
#Ilya Y. Zhbannikov, i.zhbannikov@gmail.com
#Date: 2013-12-16

library(rmetamp)

#=======Step 1: Read the 16S aligned reference genes (sequences)=======#
#Loading the distance matrix of ref. 16S and computing MDS:
#A distance matrix can be also created from aligned MSA file. 
mds16S <- ComputeMDS(system.file("extdata","L16S_aligned.fasta",package="rmetamp"))#,mode="dist")
#Visualize
plot_mds(mds16S,caption="2D MDS, whole 16S",col="green")
#=======Step 2: Read the distance matrices from variable regions=======#
#mdsV4 <- ComputeMDS(system.file("extdata","LV4_19sp.pim",package="rmetamp"),mode="dist")
mdsV4 <- ComputeMDS(system.file("extdata","LV4_aligned.fasta",package="rmetamp"))
#======Step 3: Generate the test set=============#
q4 <- generate_test_set(mdsV4,50, limits=c(0.001,0.001))
#add unknown species:
#q4<- rbind(q4, matrix(runif(100,min=-2.50,max=-2.0),nrow=50,ncol=2))
q4<- rbind(q4, matrix(runif(100,min=-0.025,max=-0.020),nrow=50,ncol=2))
ans <- process_amplicon_data( mds16S, mdsV4, q4)
mdsV4_new <- ans[[1]]
q4_new <- ans[[2]]

#Now do the same with v-3 region:
#mdsV3 <- ComputeMDS(system.file("extdata","LV3_19sp.pim",package="rmetamp"),mode="dist")
mdsV3 <- ComputeMDS(system.file("extdata","LV3_aligned.fasta",package="rmetamp"))
q3 <- generate_test_set(mdsV3,50, limits=c(0.001,0.001))
#add unknown species:
#q3<- rbind(q3, matrix(runif(100,min=-2.70,max=-2.5),nrow=50,ncol=2))
q3<- rbind(q3, matrix(runif(100,min=-0.027,max=-0.025),nrow=50,ncol=2))
ans <- process_amplicon_data( mds16S, mdsV3, q3)
mdsV3_new <- ans[[1]]
q3_new <- ans[[2]]

#Now do the same with v-5 region:
#mdsV5 <- ComputeMDS(system.file("extdata","LV5_19sp.pim",package="rmetamp"),mode="dist")
mdsV5 <- ComputeMDS(system.file("extdata","LV5_aligned.fasta",package="rmetamp"))
q5 <- generate_test_set( mdsV5,50,limits=c(0.001,0.001) )
ans <- process_amplicon_data( mds16S, mdsV5, q5)
mdsV5_new <- ans[[1]]
q5_new <- ans[[2]]

#===========Visualize============#
plot_mds(mdsV4[1:16,],caption="V4 MDS, before transformation",col='blue')
points(q4,col="black",cex=0.1)
legend("bottomleft", pch=19,legend=c("reference","empirical"), col = c("blue", "black"))
plot_mds(mdsV3[1:16,],caption="V3 MDS, before transformation",col='red')
points(q3,col="black",cex=0.1)
legend("bottomleft", pch=19,legend=c("reference","empirical"), col = c("red", "black"))
plot_mds(mdsV5[1:16,],caption="V5 MDS, before transformation",col='yellow')
points(q5,col="black",cex=0.1)
legend("bottomright", pch=19,legend=c("reference","empirical"), col = c("yellow", "black"))
#Summary (for v3,4 and 5):
mdsV4_new1 <- mdsV4_new[1:dim(mdsV4)[1],]
rownames(mdsV4_new1) <- rownames(mdsV4) 
plot_mds(mdsV4_new1,col="green",caption="2D MDS V4,V3,V5 reference and empirical points after transformation")
text(mdsV4_new1, rownames(mdsV4_new1), cex = 0.6,pos=3)
legend("top", pch=19,legend=c("reference","V4-empirical","V3-empirical","V5-empirical"), cex=0.5, col = c("green", "blue", "red", "yellow"))
points(q4_new, col="blue",cex=0.1)
points(q3_new, col="red",cex=0.1)
points(q5_new, col="yellow",cex=0.1)
}
\keyword{Meta-amplicon data analysis}